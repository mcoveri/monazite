#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE "baseTest"
#include <boost/test/unit_test.hpp>
#include "src/data_readers/MRead.hpp"

BOOST_AUTO_TEST_SUITE( base_reader_tests )

/* dummy test for test/demonstration */
int i = 2;
BOOST_AUTO_TEST_CASE( dummy ){
    int j = 2;
    BOOST_CHECK(i == j);
}

/*
 * Ensures data is properly read from hdf5 and json files
 * given the nuclide name or nuclide zaid is used to look it up
 */
baseReader bRead;
BOOST_AUTO_TEST_CASE( zaid_and_name_read ){
    // u235 check
    BOOST_CHECK_CLOSE(bRead.getData("u235").m, 235.0439, 0.001);
    BOOST_CHECK_CLOSE(bRead.getData(92235).m, 235.0439, 0.001);
    BOOST_CHECK_CLOSE(bRead.getData(922350000).m, 235.0439, 0.001);
    // oxygen16 check
    BOOST_CHECK_CLOSE(bRead.getData("o16").m, 15.9949, 0.001);
    BOOST_CHECK_CLOSE(bRead.getData(8016).m, 15.9949, 0.001);
    BOOST_CHECK_CLOSE(bRead.getData(80160000).m, 15.9949, 0.001);
}

BOOST_AUTO_TEST_SUITE_END()
