#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE "pinCellMixTest"
#include <boost/test/unit_test.hpp>
#include "src/monazite/MMat.hpp"

BOOST_AUTO_TEST_SUITE( base_tests )

/*
 * test init by atom fracs and number densities 
 * for self consistency
 */
BOOST_AUTO_TEST_CASE( init_test ){
    std::map<std::string, double> isoDict = {{"h1", 2./3.}, 
                                             {"o16", 1./3.}};
    // init by atom frac
    MMat lwtrMat1(isoDict, "af");
    lwtrMat1.setDensity(1.25);
    std::map<std::string, double> isoDictMeta = {{"h1",  lwtrMat1.iso["h1"].nd},
                                                {"o16", lwtrMat1.iso["o16"].nd}};
    // init by number density
    MMat lwtrMat2(isoDictMeta, "nd");
    // must produce equiv result
    BOOST_CHECK_CLOSE(lwtrMat1.rho, lwtrMat2.rho, 1e-10);
    BOOST_CHECK_EQUAL(lwtrMat1.iso["h1"].nd, lwtrMat2.iso["h1"].nd);
    BOOST_CHECK_EQUAL(lwtrMat1.iso["h1"].af, lwtrMat2.iso["h1"].af);
    BOOST_CHECK_EQUAL(lwtrMat1.iso["o16"].nd, lwtrMat2.iso["o16"].nd);
    BOOST_CHECK_EQUAL(lwtrMat1.iso["o16"].af, lwtrMat2.iso["o16"].af);
}

/*
 * Ensures copy constructor works
 */
BOOST_AUTO_TEST_CASE( copy_test ){
    std::map<std::string, double> isoDict = {{"o16", 2./3.},
                                             {"u235", 1./3. * 0.03},
                                             {"u238", 1./3. * 0.97}};
    MMat fuelMat(isoDict, "af");
    fuelMat.setDensity(10.24);
    MMat copyFuelMat(fuelMat);
    BOOST_CHECK_EQUAL(copyFuelMat.rho, fuelMat.rho);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["o16"].nd, fuelMat.iso["o16"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["o16"].af, fuelMat.iso["o16"].af);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u235"].nd, fuelMat.iso["u235"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u235"].af, fuelMat.iso["u235"].af);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u238"].nd, fuelMat.iso["u238"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u238"].af, fuelMat.iso["u238"].af);
}

/*
 * Ensures assignment operator works
 */
BOOST_AUTO_TEST_CASE( assign_test ){
    std::map<std::string, double> isoDict = {{"o16", 2./3.},
                                             {"u235", 1./3. * 0.03},
                                             {"u238", 1./3. * 0.97}};
    MMat fuelMat(isoDict, "af");
    fuelMat.setDensity(10.24);
    MMat copyFuelMat = fuelMat;
    BOOST_CHECK_EQUAL(copyFuelMat.rho, fuelMat.rho);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["o16"].nd, fuelMat.iso["o16"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["o16"].af, fuelMat.iso["o16"].af);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u235"].nd, fuelMat.iso["u235"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u235"].af, fuelMat.iso["u235"].af);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u238"].nd, fuelMat.iso["u238"].nd);
    BOOST_CHECK_EQUAL(copyFuelMat.iso["u238"].af, fuelMat.iso["u238"].af);
}

/*
 * ensures mult operator overload works
 */
BOOST_AUTO_TEST_CASE( mult_overload_test ){
    std::map<std::string, double> isoDict = {{"h1", 2./3.}, 
                                             {"o16", 1./3.}};
    MMat lwtrMat1(isoDict, "af");
    lwtrMat1.setDensity(1.0);
    BOOST_CHECK_CLOSE((lwtrMat1 * 0.5).rho, 0.5, 1e-8);
}

/*
 * Ensures addition overloaded operator works
 */
BOOST_AUTO_TEST_CASE( plus_overload_test ){
    std::map<std::string, double> isoDict = {{"h1", 2./3.}, 
                                             {"o16", 1./3.}};
    MMat lwtrMat1(isoDict, "af");
    lwtrMat1.setDensity(1.0);
    MMat lwtrMat2(isoDict, "af");
    lwtrMat2.setDensity(1.0);
    // half of each by vol result in the original material  density
    MMat sumMat = lwtrMat1 * 0.5 + lwtrMat2 * 0.5;
    BOOST_CHECK_CLOSE(sumMat.rho, 1.0, 1e-8);
}

BOOST_AUTO_TEST_SUITE_END()
