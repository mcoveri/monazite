#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE "pinCellMixTest"
#include <boost/test/unit_test.hpp>
#include "src/monazite/MMat.hpp"


BOOST_AUTO_TEST_SUITE( pincell_mix_suite )

/*
 * Ensure that both material mixture constructors
 * produce same result. Can construct materials by
 * zaid or by nuclide string names.
 */
BOOST_AUTO_TEST_CASE( zaid_v_name ){
    std::map<int, double> isoDictZaid = {{1001, 2./3.}, 
                                         {8016, 1./3.}};
    MMat lwtrMatZaid(isoDictZaid, "af");
    lwtrMatZaid.setDensity(1.0);
    std::map<std::string, double> isoDictName = {{"h1", 2./3.}, 
                                                 {"o16", 1./3.}};
    MMat lwtrMatName(isoDictName, "af");
    lwtrMatName.setDensity(1.0);
    BOOST_CHECK_EQUAL(lwtrMatName.rho , lwtrMatZaid.rho);
    BOOST_CHECK_EQUAL(lwtrMatName.iso["o16"].nd , lwtrMatName.iso[8016].nd);
    BOOST_CHECK_EQUAL(lwtrMatZaid.iso["o16"].nd , lwtrMatZaid.iso[8016].nd);
    BOOST_CHECK_EQUAL(lwtrMatName.iso["h1"].nd , lwtrMatName.iso[1001].nd);
    BOOST_CHECK_EQUAL(lwtrMatZaid.iso["h1"].nd , lwtrMatZaid.iso[1001].nd);
    BOOST_CHECK_EQUAL(lwtrMatName.iso["h1"].nd , lwtrMatZaid.iso[1001].nd);
    BOOST_CHECK_EQUAL(lwtrMatName.iso["o16"].nd , lwtrMatZaid.iso[8016].nd);
}

/*
 * Compare nuclide number densities to known values
 * for a pin cell configuration.
 */
BOOST_AUTO_TEST_CASE( pin_cell_1 ){
    std::map<int, double> isoDict = {{1001, 2./3.}, 
                                     {8016, 1./3.}};
    MMat lwtrMat(isoDict, "af");
    lwtrMat.setDensity(1.0);
    // 3% enriched fuel material
    std::map<std::string, double> isoDict2 = {{"o16", 2./3.},
                                             {"u235", 1./3. * 0.03},
                                             {"u238", 1./3. * 0.97}};
    MMat fuelMat(isoDict2, "af");
    fuelMat.setDensity(10.24);
    // homogenized material
    MMat mixedMat = fuelMat * 0.2 + lwtrMat * 0.8;
    BOOST_CHECK_CLOSE(mixedMat.rho, 2.848, 1e-8);
    BOOST_CHECK_CLOSE(mixedMat.iso["u235"].nd, 0.00013706234647, 1e-4);
    BOOST_CHECK_CLOSE(mixedMat.iso["u238"].nd, 0.00443168253595, 1e-4);
    BOOST_CHECK_CLOSE(mixedMat.iso["h1"].nd, 0.0534987449412362, 1e-4);
    BOOST_CHECK_CLOSE(mixedMat.iso["o16"].nd, 0.035886862235471, 1e-4);
}

BOOST_AUTO_TEST_SUITE_END()
