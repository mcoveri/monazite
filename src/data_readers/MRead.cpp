#include "MRead.hpp"

#ifndef H5_NO_NAMESPACE
    using namespace H5;
    #define H5_NO_NAMESPACE
#endif

baseReader::baseReader(){
    // ex read hdf5 file
    // full path
    std::string buf(monazite_DATA_PATH);
    buf.append("/nuc_data.h5");
    const H5std_string FILE_NAME(buf);
    const H5std_string DATASET_NAME("atomic_mass");
    H5File file1(FILE_NAME, H5F_ACC_RDONLY);
    DataSet dataset1 = file1.openDataSet(DATASET_NAME);
    DataSpace filespace = dataset1.getSpace();

    // create data space size
    CompType mtype1(sizeof(bData));

    // data member names
    const H5std_string MEMBER1("nuc");
    const H5std_string MEMBER2("mass");
    const H5std_string MEMBER3("error");
    const H5std_string MEMBER4("abund");
    mtype1.insertMember( MEMBER1, HOFFSET(bData, zaid), PredType::NATIVE_INT);
    mtype1.insertMember( MEMBER2, HOFFSET(bData, m), PredType::NATIVE_DOUBLE);
    mtype1.insertMember( MEMBER3, HOFFSET(bData, mUncert), PredType::NATIVE_DOUBLE);
    mtype1.insertMember( MEMBER4, HOFFSET(bData, natf), PredType::NATIVE_DOUBLE);

    // read data into baseData
    dataset1.read(baseData, mtype1);

    // read json file
    ascData();

    //
    std::cout << "Completed basic database construction." << std::endl;
}

void baseReader::ascData(){
    std::string fpath(monazite_DATA_PATH);
    fpath.append("/isoDict.json");
    
    boost::property_tree::ptree zdata;
    boost::property_tree::read_json(fpath, zdata);
    int testzaid;
    std::string testname;
    for(auto iter : zdata){
        testzaid = iter.second.get<int>("zaid", 0);
        testzaid = testzaid * 10000;
        testname = iter.first;
        matchbData(testzaid, testname);
    }
}

void baseReader::matchbData(int testzaid, std::string testname){
    for(bData &nuclideData : baseData){
        if(nuclideData.zaid == testzaid)
            nuclideData.name = testname;
    }
}

template<class T>
const bData &baseReader::getData(T){
    throw std::domain_error("FATAL ERROR: nuclide-lookup by supplied data type not supported.");
    return(grabDataByZaid(1001));
}
template<>
const bData &baseReader::getData<int>(int inzaid){
    return(grabDataByZaid(inzaid));
}
template<>
const bData &baseReader::getData<std::string>(std::string inname){
    return(grabDataByName(inname));
}
template<>
const bData &baseReader::getData<const char *>(const char *inname){
    return(grabDataByName(inname));
}

const bData &baseReader::grabDataByName(std::string inname){
    for(const bData &nuclideData : baseData){
        if (nuclideData.name == inname)
            return nuclideData;
    }
    throw std::domain_error("nuclide not found in basic data");
}

const bData &baseReader::grabDataByZaid(int zin){
    try{
        int zzaaammmm = zin * 10000;
        return searchDataByZaid(zzaaammmm);
    }catch (std::domain_error){
        std::cout << "long_zaid lookup." << std::endl;
        return searchDataByZaid(zin);
    }
    throw std::domain_error("nuclide not found in basic data");
}

const bData &baseReader::searchDataByZaid(int z2a3m4){
    for(const bData &nuclideData : baseData){
        if (nuclideData.zaid == z2a3m4)
            return nuclideData;
    }
    throw std::domain_error("zaid not found in basic data");
}

baseReader::~baseReader(){}
