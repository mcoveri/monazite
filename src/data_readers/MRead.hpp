#ifndef INCLUDE_MREAD_H
#define INCLUDE_MREAD_H
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "hdf5.h"
#include "H5Cpp.h"
#include <iostream>
#include <stdexcept>
#include "monaziteConfig.h"

#ifndef H5_NO_NAMESPACE
    using namespace H5;
    #define H5_NO_NAMESPACE
#endif

// basic isotope data struct
typedef struct bData{
    int zaid;
    double m;
    double mUncert;
    double natf;
    std::string name;
} bData;

class baseReader
{
    public:
        /* pub vars */
        bData baseData[3466];

        /* constructor */
        baseReader();

        /* public member funcs */
        template<class T>
        const bData &getData(T /*p*/);

        /* destructor */
        ~baseReader();

    private:
        const bData &grabDataByZaid(int /*p*/);
        const bData &grabDataByName(std::string /*p*/);
        void ascData();
        void matchbData(int, std::string);
        const bData &searchDataByZaid(int /*p*/);
};

#endif
