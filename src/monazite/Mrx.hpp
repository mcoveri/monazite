#ifndef INCLUDE_MRX_H
#define INCLUDE_MRX_H
#include <vector>
#include <list>
#include <string>
#include <utility>

/* 
 * Contans:
 * - Rxn XS table data Ex.  (n, gamma) multigroup xs table.
 * - number of groups required to utilize table data.
 * - specilized reactoin channel functions.
 * - this is meant to be extended by user if specific
 *   reaction channels are needed or if special post
 *   processors are desired.
 *
 * Note:
 *   Some data fields are only useful for transmutation calculatoins
 *   such as branching ratios or reaction daughters.
 *   See monaburn/Dch.hpp for decay channel class which is similar, but
 *   has no use for fluxs, MT numbers, or cross section data.
 */
template<typename T>
class RxnChannel{
    public:
        double nG;  // number of groups
        int MT;     // MT number of reaction channel, see NJOY docs.
        std::vector<int>  daughters;    // zaid(s) of daughters 
        std::vector<double> bratios;    // branch ratios of each daughter path
        std::pair<char, char> rxnpair;  // in particle, out particle pair eg: ('n', 'a') for (n, alpha) rxn
        double Q = 0.;   // energy release per rxn

        // xs table storage
        T xs;

        /* Constructor:
         * Unknown cross section data table type.  could be:
         * - a non std vector or matrix type like a libBoost.uBlas vec
         * - n-dimensional vector
         * - a double (if we only have xs data for a single group)
         */
        RxnChannel(int g, T xsdata) 
            :xs(xsdata), nG(g)
        {};  // accepts ngroup int and xs data

        // multiply xs by flux in each group
        T computeRR(std::vector<double> flux);

        // all children should implement their own post processor.
        virtual void post();
};


/*
 * lumped n, dissapearance data
 */
template<typename T>
class nAbs : public RxnChannel<T>{
    public:
        std::pair<char, char> rxnpair ={'n', 'g'};
        int MT = 3;
};

/*
 * lumped nuFission data
 */
template<typename T>
class nNuFission : public RxnChannel<T>{
    public:
        std::pair<char, char> rxnpair ={'n', 'f'};
        const double Q = 200;  // energy per fission in MeV
        int MT = 18;
};

/*
 * lumped scatter
 */
template<typename T>
class nScatter : public RxnChannel<T>{
    public:
        std::pair<char, char> rxnpair ={'n', 'n'};
        int MT = 8;
};

#endif
