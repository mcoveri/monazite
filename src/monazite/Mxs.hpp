#ifndef INCLUDE_MXS_H
#define INCLUDE_MXS_H
#include <vector>
#include <list>
#include <map>
#include <string>
#include <utility>
#include "Mrx.hpp"

/*
 * Provides the capability to build a microscopic xs table
 * from an arbirtray collection of reaction channels.  
 *
 * Many reaction channels can comprise a single cross section
 * in a transport calc.  For Example:
 * In a transport calc we dont really care
 * about the specifics of what goes into the (n, fission)[MT18] XS
 * we lump all (n, 2n), (n, 3n), and (n, f) all into one
 * XS table.
 */
class MicroXs{
    public:
        /* Constructor:
         * Depending on the data avalible in the tables, different reaction
         * channels will be avalible for each isotope.
         * The simplest situation is one generic reaction channel per microXs.
         */
        MicroXs();

        // container for reaction channels
        // template <class T>
        // static std::map<std::string, RxnChannel<T>>  rxnVector;

        // set flux (only used for transmutation and burn calcs)
        void setFlux(std::vector<double>);

        // compute all reaction rates by default.
        // specify list of reaction channel names to compute subset of rxn rates
        void crr(std::vector<double>, std::list<std::string> = {"all"});

        // call rxn rate post processors
        void post();

        // overload + operator to allow creation of total XS from
        // many other XS data sets.
        MicroXs operator+(MicroXs);
};

#endif
