#include <memory>
#include "MIso.hpp"

MIso::MIso(bData nucData)
    :zaid(nucData.zaid), isoName(nucData.name.c_str()),
     natf(nucData.natf), m(nucData.m)
{
    pullXSdata();
}

void MIso::setnd(double ndi){
    nd = ndi;
}
void MIso::setaf(double afi){
    af = afi;
}
void MIso::setmf(double mfi){
    mf = mfi;
}

bool MIso::operator<(const MIso& rhs) const
{
    return this->zaid < rhs.zaid;
}

void MIso::pullXSdata(){
}
