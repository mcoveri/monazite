#include <map>
#include "MIso.hpp"
#include "IsoMap.hpp"

IsoMap::IsoMap(){}

IsoMap::IsoMap(std::map<MIso, double> &inMap){
    for(std::map<MIso, double>::iterator iter=inMap.begin(); iter != inMap.end(); ++iter){
        iVec.push_back(iter->first);
        iW.push_back(iter->second);
        iZa.push_back(iter->first.zaid);
    }
}

void IsoMap::addMIsoMap(std::vector<MIso> otherVec){
    for(std::vector<MIso>::iterator other_iter=otherVec.begin(); 
            other_iter != otherVec.end(); ++other_iter){
        auto matchingIter = std::find(iZa.begin(), iZa.end(), other_iter->zaid);
        if( matchingIter != iZa.end()){
            // match found
            auto matchingIdx = std::distance(iZa.begin(), matchingIter);
            iW[matchingIdx] += other_iter->nd;
        }else{
            iVec.push_back(*other_iter);
            iW.push_back(other_iter->nd);
            iZa.push_back(other_iter->zaid);
        }
    }
}

/* returns MIso object given a string OR a zaid int
 * careful, we can get NULL back in which case the iso was not found: warn user but
 * do not fail. */
template<typename F>
MIso &IsoMap::operator[](F){
    throw std::domain_error("Bad type");
    return *iVec.begin();
}
template<>
MIso &IsoMap::operator[]<int>(int zin){
    if(zin < 100000){
        zin *= 10000;
    }
    for(std::vector<MIso>::iterator iter=iVec.begin(); iter != iVec.end(); ++iter){
        if((*iter).zaid == zin){
            return *iter;  // return 'address of' MIso object
        }
    }
    throw std::domain_error("zaid key not found in isotopic vector.");
}
template<>
MIso &IsoMap::operator[]<const char*>(const char* isoName){
    for(std::vector<MIso>::iterator iter=iVec.begin(); iter != iVec.end(); ++iter){
        if(std::strcmp((*iter).isoName, isoName) == 0){
            return *iter;  // return 'address of' MIso object
        }
    }
    throw std::domain_error("nuclide name key not found in isotopic vector.");
}

void IsoMap::resetiW(std::vector<double> newiW){
    iW = newiW;
}
