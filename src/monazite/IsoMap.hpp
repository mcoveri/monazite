#ifndef INCLUDE_ISOMAP_H
#define INCLUDE_ISOMAP_H
#include <map>
#include "MIso.hpp"

/*
 * An IsoMap can be accessed in a few ways:
 *      - by zaid:
 *      IsoMap[<int>] ;  // where <int> is a zaid
 *      - by string:
 *      IsoMap[<string>] ; // where <string> is something like "U235"
 */
class IsoMap {
    public:
        IsoMap();
        IsoMap(std::map<MIso, double> &);

        std::vector<MIso> iVec;  // vector of isotopes
        std::vector<int> iZa;    // vector of zaids

        // storage for isotope weights
        std::vector<double> iW;
        void resetiW(std::vector<double>);

        // add incomming MIso map to an existing one
        void addMIsoMap(std::vector<MIso>);

        // overload [] 
        template<typename F>  // int zaid, or string isoName capability
        MIso &operator[](F);

    private:
};

#endif
