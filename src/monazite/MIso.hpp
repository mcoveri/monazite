#ifndef INCLUDE_MISO_H
#define INCLUDE_MISO_H
#include <iostream>
#include <vector>
#include <map>
#include "Mxs.hpp"
#include "../data_readers/MRead.hpp"

/*
 * Stores cross section data, reaction channels and 
 * basic isotope facts like zaid, atomic mass, ect.
 */
class MIso
{
    friend class IsoMap;
    public:
        /* PUB VARS */
        bool bMasked = false;   // toggle to effectively set all XS to 0 for this isotope
        bool bFfactor = false;  // toggle to enable ffactor interp for this isotope
        int transMode = 0; // transmutation mode
        double ffactor;  // RI table parameter
        double bXs;  // background XS
        double nd;  // number density of isotope in [#/b-cm]
        double mf;  // mass fraction
        double af;  // atomic fraction

        /* PUB CONSTS */
        const int zaid;
        const char* isoName;
        const double natf;  // natural abundance fraction
        const double m;    // atomic mass

        /* Constructor */
        MIso(bData);

        /*
         * Overload < operator
         * In order to use an instance of class as a key in a dict we need to
         * overload the < operator. We need a way to tell if the keys
         * are the same or different so that the map can overwrite entries
         */
        bool operator<(const MIso& rhs) const;

        // storage for "microscopic" cross section data
        std::map<std::string, MicroXs> microProp;

        /*
         * Pulls avalible cross section data from nuc_data_pool
         * utilizing the data readers provided in Monazite, 
         * searches the data_pool for entries that match the zaid/isoName
         */
        void pullXSdata();

        // set data into microProp(s)
        void setXSdataI();

        // setters
        void setnd(double);
        void setaf(double);
        void setmf(double);

    private:
};

#endif
