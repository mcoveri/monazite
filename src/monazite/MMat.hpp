#ifndef INCLUDE_MMAT_H
#define INCLUDE_MMAT_H
#include <iostream>
#include <map>
#include "MIso.hpp"
#include "IsoMap.hpp"
#include <vector>
#include "../data_readers/MRead.hpp"

#ifndef NA
#define NA 6.02214086e23
#endif


// base data reader
#ifndef BASE
#define BASE
std::unique_ptr<baseReader> bRead = std::make_unique<baseReader>();
#endif

class MMat
{
    public:
        /* constructors */
        MMat(); 
        // construct by nuc name
        MMat(std::map<std::string, double>, 
                std::string iM= "nd", 
                std::string n = "defaultMat");
        // construct by nuc zaid
        MMat(std::map<int, double>, 
                std::string iM= "nd", 
                std::string n = "defaultMat");
        // copy constructor
        MMat(const MMat &);

        // base data reader
        //baseReader bRead;

        /* pub vars */
        std::map<MIso, double> isoM;
        std::string name, mMethod;  // name and input mixture method.
        IsoMap iso;
        const std::vector<double> *wvec = &iso.iW;   // input weight vector
        std::vector<int> &zaid = iso.iZa;     // isotope zaids vector
        std::vector<double> mf;  // mass fraction
        std::vector<double> af;  // atom fraction
        std::vector<double> nd;  // number density
        double rho = 1.0;        // mixture density
        double mixM;             // mixture avg atomic mass
    
        /* Isotope iterator */
        typedef std::vector<MIso>::iterator iterator;
        typedef std::vector<MIso>::const_iterator const_iterator;
        iterator begin() { return iso.iVec.begin(); }
        const_iterator begin() const { return iso.iVec.begin(); }
        iterator end() { return iso.iVec.end(); }
        const_iterator end() const { return iso.iVec.end(); }

        // remove isotopes (accepts vector of isotope names zaid or iso string)
        void remIso(std::vector<std::string>);

        /* change mixture density */
        void setDensity(double);
        void computeDensity();

        /* overloaded operators */
        // const function def means operator will not change any
        // member variables of *this.
        MMat operator*(double) const;
        MMat operator+(const MMat &) const;
        const MMat &operator=(const MMat&);

        /* destructor */
        ~MMat();

    private:
        // update material database
        void updateDB();

        // a bunch of helper funcs
        void normAf();
        void checkWvec();
        void wf2af();
        void af2wf();
        void setData();
        void populateWeights();
        void computeMixtureM();
        void computeAf();
        void computeNd();
        void newIsoUpdate();
        void buildNewMMat();
};
#endif
