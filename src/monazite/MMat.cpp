#include "MMat.hpp"

MMat::MMat(std::map<std::string, double> mapMIso, std::string iM, std::string n)
    :name(n), mMethod(iM)
{
    // create isotopes
    for(auto iter = mapMIso.begin(); iter != mapMIso.end(); ++iter){
        isoM[MIso(bRead->getData(iter->first))] = iter->second;
    }
    buildNewMMat();
    computeDensity();
}
MMat::MMat(std::map<int, double> mapMIso, std::string iM, std::string n)
    :name(n), mMethod(iM)
{
    // create isotopes
    for(auto iter = mapMIso.begin(); iter != mapMIso.end(); ++iter){
        isoM[MIso(bRead->getData(iter->first))] = iter->second;
    }
    buildNewMMat();
    computeDensity();
}

void MMat::buildNewMMat(){
    iso = IsoMap(isoM);
    checkWvec();
    if(mMethod == "mf"){
        // input isotopes weighted by mass frac
        af.resize(wvec->size());
        nd.resize(wvec->size());
        mf = *wvec;
        wf2af();
        normAf();
        computeMixtureM();
        computeNd();
    }else if(mMethod == "af"){
        // input isotopes weighted by atom frac
        mf.resize(wvec->size());
        nd.resize(wvec->size());
        af = *wvec;
        normAf();
        computeMixtureM();
        computeNd();
        af2wf();
    }else{
        // input by number density
        af.resize(wvec->size());
        mf.resize(wvec->size());
        nd = *wvec;
        computeAf();
        normAf();
        computeMixtureM();
        af2wf();
    }
    // standardize all wvecs to number density
    mMethod = "nd";
    //iso.iW = nd;
    //*wvec = nd;
    this->iso.resetiW(nd);
    // populate data fields in all isotopes in the mix
    setData();
}

void MMat::newIsoUpdate(){
    af.resize(wvec->size());  // resize vectors to make room for new isotope data
    mf.resize(wvec->size());
    nd = *wvec;
    computeAf();
    normAf();
    computeMixtureM();
    af2wf();
    computeDensity();
    setData();
}

// compute atomic fractions from number densities
void MMat::computeAf(){
    int i = 0;
    double sumNd = std::accumulate(nd.begin(), nd.end(), 0.0);
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        af[i] = nd[i] / sumNd;
        ++i;
    }
}

void MMat::normAf(){
    double sumAf = std::accumulate(af.begin(), af.end(), 0.0);
    for(std::vector<double>::iterator iter = af.begin(); iter != af.end(); ++iter){
        *iter /= sumAf;
    }
}

// AF * rho (g/cc) * NA (#/mol) / M (g/mol) = #atoms/cm^3
void MMat::setDensity(double density){
    rho = density;
    updateDB();
}

void MMat::computeNd(){
    for(unsigned i = 0; i < wvec->size(); ++i){
        nd[i] = rho * af[i] * NA / mixM / 1.0e24;
    }
}

/* Updates the material database when certain actions are performed. */
void MMat::updateDB(){
    computeNd();
    this->iso.resetiW(nd);
    computeAf();
    normAf();
    computeMixtureM();
    af2wf();
    setData();
}

// converters
void MMat::wf2af(){
    double aSum = 0.;
    int i = 0;
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        aSum += mf[i] * (1. / iso_iter->m) * NA;
        ++i;
    }
    i = 0;
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        af[i] = mf[i] * (1. / iso_iter->m) * NA / aSum;
        ++i;
    }
}
void MMat::af2wf(){
    double wfSum = 0.;
    int i = 0;
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        wfSum += af[i] * (1. / NA) * iso_iter->m;
        ++i;
    }
    i = 0;
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        mf[i] = af[i] * (1. / NA) * iso_iter->m  / wfSum;
        ++i;
    }
}

// given we have number densites; compute mixed material density
void MMat::computeDensity(){
    double density = 0.;
    int i = 0;
    // af * iso_iter->m == M
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        density += mixM * nd[i] * 1.0e24 / NA / af[i];
        ++i;
    }
    this->rho = density / wvec->size();
}

void MMat::computeMixtureM(){
    mixM = 0.;
    int i = 0;
    for(auto iso_iter = this->begin(); iso_iter != this->end(); ++iso_iter){
        mixM += af[i] * iso_iter->m;
        ++i;
    }
}

// Ensures no weight is less than zero
void MMat::checkWvec(){
    for(std::vector<double>::const_iterator iter = wvec->begin(); iter != wvec->end(); ++iter){
        if(*iter < 0){
            throw std::runtime_error("Isotopic fraction < 0 is not allowed in mixture.");
        }
    }
}

// initilize atom, mass, and number density arrays
void MMat::populateWeights(){
    if(mMethod == "af"){
        af = *wvec;
    }else if(mMethod == "mf"){
        mf = *wvec;
    }else{
        nd = *wvec;
    }
}

// Sets data into isotope instances
void MMat::setData(){
    for(unsigned int i = 0; i < wvec->size(); ++i){
        iso[zaid[i]].setnd(nd[i]);
        iso[zaid[i]].setaf(af[i]);
        iso[zaid[i]].setmf(mf[i]);
    }
}

MMat MMat::operator*(double volMult) const{
    MMat tmpThis = *this;
    tmpThis.rho *= volMult;
    tmpThis.updateDB();
    return tmpThis;
}

MMat MMat::operator+(const MMat &other) const{
    MMat tmpThis = *this;  // make a temporary copy of *this
    tmpThis.iso.addMIsoMap(other.iso.iVec);
    tmpThis.newIsoUpdate();
    //tmpThis.updateDB();
    return tmpThis;  // return *this with added other isotopes
}

// copy constructor
MMat::MMat(const MMat &other){
    this->isoM = other.isoM;
    this->iso.iW = other.iso.iW;
    this->rho = other.rho;
    buildNewMMat();
    updateDB();
}

// assignment operator
const MMat &MMat::operator=(const MMat &other){
    if(this == &other){
        return *this;
    }
    this->isoM = other.isoM;
    this->iso.iW = other.iso.iW;
    this->rho = other.rho;
    buildNewMMat();
    updateDB();
    return *this;
}

MMat::~MMat(){}
