NJOY XS generators
==================

1. Grab endfvii.0 xs from the lanl T2 website with the endfcrawl.py script:

    $python endfcrawl.py

2. Generate XS files:

    $./createXS.sh

3. Generate HDF5 XS data file for Monazite:

    $python makeH5XS.py default_XS

By default this generates a `xsdefault.xs.h5` file that has a few cross sections.

To add more cross sections: modify the `createXS.sh` file to
run NJOY and generate multigroup XS files for all isotopes of interest.

Group structure, Doppler temperature and other parameters can be adjusted in the
`njoybatchGRP*` files.
