#include "monazite/MMat.hpp"
#include "monazite/MIso.hpp"
#include "monaziteConfig.h"
#include "data_readers/MRead.hpp"
#include <memory>
#include <map>
#include <list>
#include <iostream>

int main(){
    // create mixture
    std::map<int, double> isoDict = {{1001, 2./3.}, 
                                     {8016, 1./3.}};
    MMat lwtrMat(isoDict, "af");
    lwtrMat.setDensity(1.0);

    // get mixture facts
    std::cout << "The atom density of h1 in lwtr is: " << lwtrMat.iso["h1"].nd << std::endl;
    std::cout << "The atom density of o16 in lwtr is: " << lwtrMat.iso["o16"].nd << std::endl;

    // fuel material
    std::map<std::string, double> isoDict2 = {{"o16", 2./3.},
                                             {"u235", 1./3. * 0.03},
                                             {"u238", 1./3. * 0.97}};
    MMat fuelMat(isoDict2, "af");
    fuelMat.setDensity(10.24);
    std::cout << "Density of raw fuel is: " << fuelMat.rho << std::endl;
    std::cout << "The atom density of o16 in raw fuel is: " << fuelMat.iso["o16"].nd << std::endl;
    std::cout << "The atom density of u235 in raw fuel is: " << fuelMat.iso["u235"].nd << std::endl;
    std::cout << "The atom density of u238 in raw fuel is: " << fuelMat.iso["u238"].nd << std::endl;
    // homogenized material
    MMat mixedMat = fuelMat * 0.2 + lwtrMat * 0.8;
    std::cout << "Density of mixutre is: " << mixedMat.rho << std::endl;
    std::cout << "The atom density of h1 in mix is: " << mixedMat.iso["h1"].nd << std::endl;
    std::cout << "The atom density of o16 in mix is: " << mixedMat.iso["o16"].nd << std::endl;
    std::cout << "The atom density of u238 in mix is: " << mixedMat.iso["u238"].nd << std::endl;
    std::cout << "The atom density of u235 in mix is: " << mixedMat.iso["u235"].nd << std::endl;

    // new material by number density
    std::map<std::string, double> isoDict3 = {{"h1", mixedMat.iso["h1"].nd},
                                             {"o16", mixedMat.iso["o16"].nd},
                                             {"u238", mixedMat.iso["u238"].nd},
                                             {"u235", mixedMat.iso["u235"].nd}};
    MMat fuelNDMat(isoDict3, "nd");
    std::cout << "Density of mixutre constructed from num density is: " << fuelNDMat.rho << std::endl;
    std::cout << "The atom density of h1 in mix is: " << fuelNDMat.iso["h1"].nd << std::endl;
    std::cout << "The atom density of o16 in mix is: " << fuelNDMat.iso["o16"].nd << std::endl;
    std::cout << "The atom density of u238 in mix is: " << fuelNDMat.iso["u238"].nd << std::endl;
    std::cout << "The atom density of u235 in mix is: " << fuelNDMat.iso["u235"].nd << std::endl;

    // finish
    std::cout << "Exited with status 0" << std::endl;
    return 0;
}
